[NoInterfaceObject] interface GlobalSimpleDB {
  readonly attribute SimpleDBFactory simpleDB;
};
Window implements GlobalSimpleDB;
Worker implements GlobalSimpleDB;

interface SimpleDBFactory {
  Promise<SimpleDB> open(DOMString name);
  Promise<void> delete(DOMString name);
  // TODO: Enumerate?
};

// Same as Indexed DB keys:
typedef (double or Date or DOMString or sequence<SimpleDBKey>) SimpleDBKey;
typedef IDBKeyRange SimpleDBKeyRange;

enum SimpleDBIterationDirection {
    "forward",
    "reverse"
};

// TODO: Allow key-only iteration.
dictionary SimpleDBForEachOptions {
  SimpleDBKeyRange range = null;
  SimpleDBIterationDirection direction = "forward";
};

dictionary SimpleDBKeyValue {
  SimpleDBKey key;
  any value;
};

callback SimpleDBForEachCallback = bool (SimpleDBKey key, optional any value);

interface SimpleDB {
  static short cmp(SimpleDBKey a, SimpleDBKey b);

  Promise<any> get(SimpleDBKey key);
  Promise<void> set(SimpleDBKey key, any value);
  Promise<void> delete(SimpleDBKey key);

  // TODO: Premature optimization?
  Promise<sequence<any>> getMany(sequence<SimpleDBKey> keys);
  Promise<void> setMany(sequence<SimpleDBKeyValue> entries);
  Promise<void> deleteMany(sequence<SimpleDBKey> keys);

  Promise<void> clear();

  // Can't mutate while iterating
  // Return true to terminate iteration
  Promise<void> forEach(SimpleDBForEachCallback callback, options SimpleDBForEachOptions options);

  readonly attribute DOMString name;
};
